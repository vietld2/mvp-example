package com.example.mvpexample.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.mvpexample.R
import com.example.trainingground.model.Country

class CountryAdapter(val context: Context): BaseAdapter() {
    private val listData = mutableListOf<Country>()

    fun submitList(subList: List<Country>){
        listData.clear()
        listData.addAll(subList)
    }

    override fun getCount(): Int {
        return listData.size
    }

    override fun getItem(p0: Int): Country {
        return listData.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val view = LayoutInflater.from(context).inflate(R.layout.item_country, null)

        view.findViewById<TextView>(R.id.tv_name).text = listData.get(p0).name
        view.findViewById<TextView>(R.id.tv_capital).text = listData.get(p0).capital

        return view
    }

}