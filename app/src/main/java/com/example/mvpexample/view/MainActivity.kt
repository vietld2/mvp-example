package com.example.mvpexample.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import com.example.mvpexample.R
import com.example.mvpexample.presenter.MainImp
import com.example.mvpexample.presenter.MainPresenter
import com.example.mvpexample.view.adapter.CountryAdapter
import com.example.trainingground.model.Country

class MainActivity : AppCompatActivity(), MainImp.MainView {
    private lateinit var mPresenter: MainPresenter
    private lateinit var countryAdapter : CountryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()

        mPresenter = MainPresenter(this)
        rvCountry?.adapter = countryAdapter

        btnAdd?.setOnClickListener {
            val country = Country(
                edCountryName?.text.toString(),
                edCapital?.text.toString()
            )
            listCountry.add(country)
            countryAdapter.submitList(listCountry)
            countryAdapter.notifyDataSetChanged()
        }

        btnFilter?.setOnClickListener {
            mPresenter.validateData(listCountry)
        }
    }

    private val listCountry = mutableListOf<Country>()
    override fun onValidateData(validateList: List<Country>) {
        countryAdapter.submitList(validateList)
        countryAdapter.notifyDataSetChanged()
    }

    private var edCountryName: EditText? = null
    private var edCapital: EditText? = null
    private var rvCountry: ListView? = null
    private var btnAdd: Button? = null
    private var btnFilter: Button? = null

    private fun initView() {
        edCountryName = findViewById<EditText>(R.id.ed_countryName)
        edCapital = findViewById<EditText>(R.id.ed_capital)
        rvCountry = findViewById<ListView>(R.id.rv_country)
        btnAdd = findViewById<Button>(R.id.btn_add)
        btnFilter = findViewById<Button>(R.id.btn_filter)

        countryAdapter = CountryAdapter(this)
    }
}