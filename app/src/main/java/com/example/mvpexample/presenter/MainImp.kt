package com.example.mvpexample.presenter

import com.example.trainingground.model.Country

class MainImp {
    interface MainView {
        fun onValidateData(validateList: List<Country>)
    }

    abstract class MainPresenter {
        open fun validateData(list: MutableList<Country>){{

        }}

        abstract fun validateData()
    }

    interface SecondPresenter {
        fun doingWithDatabase()
    }
}