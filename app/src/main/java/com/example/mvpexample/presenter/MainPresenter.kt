package com.example.mvpexample.presenter

import com.example.trainingground.model.Country

class MainPresenter(private val mView: MainImp.MainView) : MainImp.MainPresenter() {

    override fun validateData(list: MutableList<Country>) {
        val validateList = list.filter {
            it.name.isNotEmpty() && it.capital.isNotEmpty()
        }
        mView.onValidateData(validateList)
    }

    override fun validateData() {

    }


}

